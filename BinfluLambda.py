import sys
import boto3
import logging
import time
from binflu_aws.sqs import Sqs


class BinfluLambda(object):
    def __init__(self, env, queueName):
        self.startTime = time.time()
        self.queueName = queueName
        self.maxExecutionTime = env.get('set_priority', 240)
        self.retryLimit = env.get('set_prepeat_limit', 5)

        self.youtubeUserQueue = Sqs(queueName, env['set_priority'])
        self.initLogger()
        self.start()

    def initLogger(self):
        logging.basicConfig()
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

    def start(self):
        messages = self.youtubeUserQueue.getMessages()
        if not messages.get('Messages'):
            self.logger.info('Not new messages')
        else:
            self.processMessages(messages)
        self.close()

    def processMessages(self, messages):
        for message in messages['Messages']:
            data = self.youtubeUserQueue.getMessageData(message)
            if (isinstance(data, list)):
                print message
                self.workEach(data, message["Attributes"]["MessageDeduplicationId"])
            else:
                self.work(data)
            self.youtubeUserQueue.deleteMessage(message)

    def workEach(self, items, id):
        while items:
            if(self.hasExcededMaxExecutionTime()):
                self.youtubeUserQueue.replanMessage(items, id, self.retryLimit)
                items = []
            else:
                item = items.pop(0)
                self.work(item)

    def hasExcededMaxExecutionTime(self):
        execTime = time.time() - self.startTime
        return (execTime > self.maxExecutionTime)

    def work(self, data):
        print "BynpyWork"
        pass

    def close(self):
        pass
